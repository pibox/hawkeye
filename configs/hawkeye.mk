# ---------------------------------------------------------------
# Build $(PK)
# ---------------------------------------------------------------

root-verify:
	@if [ $(UID) -ne 0 ]; then \
		$(MSG11) "You must be root." $(EMSG); \
		exit 1; \
	fi

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

build-verify:
	@if [ "$(CROSS_COMPILER)" = "" ] || [ ! -f $(CROSS_COMPILER) ]; then \
		$(MSG11) "Can't find cross toolchain.  Try setting XI= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ "$(SD)" = "" ] || [ ! -d $(SD) ]; then \
		$(MSG11) "Can't find staging tree.  Try setting SD= on the command line." $(EMSG); \
		exit 1; \
	fi
ifeq ($(HW),rpi)
	@if [ "$(TD)" = "" ] || [ ! -d $(TD) ]; then \
		$(MSG11) "Can't find target tree.  Try setting TD= on the command line." $(EMSG); \
		exit 1; \
	fi
endif

# Retrieve package
$(PK_T)-get: .$(PK_T)-get

.$(PK_T)-get: 
	@if [ ! -f $(ARCDIR)/$(PK_ARCHIVE) ]; then \
		$(MSG) "================================================================"; \
		$(MSG2) "Retrieving files" $(EMSG); \
		$(MSG) "================================================================"; \
		mkdir -p $(ARCDIR); \
		cd $(ARCDIR) && wget $(PK_URL); \
	else \
		$(MSG3) "Source is cached" $(EMSG); \
	fi
	@touch .$(subst .,,$@)

$(PK_T)-get-patch: .$(PK_T)-get-patch

.$(PK_T)-get-patch: .$(PK_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
$(PK_T)-unpack: .$(PK_T)-unpack

.$(PK_T)-unpack: .$(PK_T)-get-patch
	@mkdir -p $(BLDDIR)
	@cd $(ARCDIR) && $(UNPACK_CMD)
	@touch .$(subst .,,$@)

# Apply patches
$(PK_T)-patch: .$(PK_T)-patch

.$(PK_T)-patch: .$(PK_T)-unpack
	@if [ -d $(DIR_PATCH) ]; then \
		$(MSG) "================================================================"; \
		$(MSG2) "Patching source" $(EMSG); \
		$(MSG) "================================================================"; \
		for patchname in `ls -1 $(DIR_PATCH)/*.patch`; do \
			$(MSG3) Applying $$patchname $(EMSG); \
			cd $(PK_SRCDIR) && patch -Np1 -r - < $$patchname; \
		done; \
	fi
	@cp $(DIR_PK)/Makefile.include $(PK_SRCDIR)/src/
	@sed -i 's%\[STAGING\]%$(STAGING)%g' $(PK_SRCDIR)/src/Makefile.include
	@sed -i 's%\[TARGETFS\]%$(TARGETFS)%g' $(PK_SRCDIR)/src/Makefile.include
	@sed -i 's%\[XI\]%$(XCC_PREFIXDIR)%g' $(PK_SRCDIR)/src/Makefile.include
	@touch .$(subst .,,$@)

$(PK_T)-init: .$(PK_T)-init 

.$(PK_T)-init:
	@make build-verify
	@make .$(PK_T)-patch
	@touch .$(subst .,,$@)

$(PK_T)-config: .$(PK_T)-config

.$(PK_T)-config:
	@touch .$(subst .,,$@)

$(PK_T): .$(PK_T)

.$(PK_T): .$(X264_T) .$(PK_T)-init 
	@make --no-print-directory $(PK_T)-config
	@$(MSG) "================================================================"
	@$(MSG2) "Building PK" $(EMSG)
	@$(MSG) "================================================================"
	@cd $(PK_SRCDIR) && make

# Build the package
	@touch .$(subst .,,$@)

$(PK_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "PK Files" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(PK_SRCDIR)

# Package it as an opkg 
pkg $(PK_T)-pkg: .$(PK_T) 
	@make --no-print-directory root-verify opkg-verify
	@mkdir -p $(PKGDIR)/opkg/$(PK)/CONTROL
	@mkdir -p $(PKGDIR)/opkg/$(PK)/etc
	@mkdir -p $(PKGDIR)/opkg/$(PK)/usr/bin
	@cp $(PK_SRCDIR)/src/hawkeye $(PKGDIR)/opkg/$(PK)/usr/bin
	@cp $(SRCDIR)/opkg/control $(PKGDIR)/opkg/$(PK)/CONTROL/control
	@cp $(SRCDIR)/opkg/postinst $(PKGDIR)/opkg/$(PK)/CONTROL/postinst
	@cp $(SRCDIR)/opkg/prerm $(PKGDIR)/opkg/$(PK)/CONTROL/prerm
	@cp $(SRCDIR)/opkg/debian-binary $(PKGDIR)/opkg/$(PK)/CONTROL/debian-binary
	@chmod +x $(PKGDIR)/opkg/$(PK)/CONTROL/postinst
	@chmod +x $(PKGDIR)/opkg/$(PK)/CONTROL/prerm
	@chown -R root.root $(PKGDIR)/opkg/$(PK)/
	@cd $(PKGDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O $(PK)
	@cp $(PKGDIR)/opkg/*.opk $(PKGDIR)/
	@rm -rf $(PKGDIR)/opkg

# Clean the packaging
pkg-clean $(PK_T)-pkg-clean:
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm -rf $(PKGDIR); fi

# Clean out a cross compiler build but not the CT-NG package build.
$(PK_T)-clean: $(PK_T)-pkg-clean
	@if [ "$(PK_SRCDIR)" != "" ] && [ -d "$(PK_SRCDIR)" ]; then \
		cd $(PK_SRCDIR) && make distclean; \
	fi
	@rm -f .$(PK_T) 

# Clean out everything associated with PK
$(PK_T)-clobber: root-verify
	@rm -rf $(PKGDIR) 
	@rm -rf $(BLDDIR)/tmp 
	@rm -rf $(PK_SRCDIR) 
	@rm -f .$(PK_T)-config .$(PK_T)-init .$(PK_T)-patch \
		.$(PK_T)-unpack .$(PK_T)-get .$(PK_T)-get-patch
	@rm -f .$(PK_T) 

