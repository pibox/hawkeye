# Build System For Embedded Boards - builds 
# an opkg for third party package.
# ----------------------------------------------------
all: defaultBB

# These files contain common variables and targets.
include config.mk
include util.mk

# ---------------------------------------------------------------
# Default build
# ---------------------------------------------------------------
defaultBB: .$(HAWKEYE_T) 

# ---------------------------------------------------------------
# Cleanup targets - seldom used since they affect all 
# components at once.
# ---------------------------------------------------------------
clean: 
	@for component in $(TARGETS); do \
		echo "Clobbering: $$component"; \
		make --no-print-directory $$component-clean; \
	done

# Careful - this wipes your archive out too!
clobber: 
	@for component in $(TARGETS); do \
		echo "Clobbering: $$component"; \
		make --no-print-directory $$component-clobber; \
	done
	@rm -f Changelog*
	@rm -rf $(ARCDIR) $(BLDDIR) $(PKGDIR)

