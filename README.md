## Synopsis

This is a metabuild system.  It's goal is to provide a build system and package generator for the current release of hawkeye for use on PiBox.

## Build

This build is based on GNU Make.  It designed to build for the following hardware platforms:

    Raspberry Pi (PiBox)

Before proceeding, read any Linux distribution-specific notes, such as "fedora.notes".  If there are none, then don't worry about it.

The build depends on some environment variables being set.  Copy the file scripts/bashsetup.sh to another directory and edit it as appropriate.  After editing, source the script:

    . <path_to_script>/bashsetup.sh

Then setup your environment by running the function in that script:

    hawkeye

Now you're ready to retrieve the source.  Run the following command to see how to clone the source tree:

    cd?

This will tell you exactly what to do.

To get additional help on available targets, use the following command:

    make help

This will display all the available targets and environment variables with explanations on how to use them.

### Cross compile and packaging

After you clone the source you can do a complete build just by typing:

    sudo make SD=<path to PiBox staging tree> pkg

You need the PiBox toolchain to build this package.  Grab it from a PiBox release archive or build it yourself.  We assume it's installed under /opt/rpiTC.  If it's not, set the XI environment variable.

    sudo make SD=<path to PiBox staging tree> XI=<path to toolchain> pkg

1. SD is the path to the PiBox root file system staging directory.
1. XI is the path to the cross compiler toolchain directory.

The first two components can be found under the respective build trees of the PiBox Development Platform build.  They may also be distributed as part of the packaged PiBox Development Platform.  Either way, the argument for these options is a directory path.

The last component requires installation of the opkg tools on the host.  These can be built from the PiBox Development Platform using the following target.

    make opkg
    make opkg-install

## Cleanup

To remove all build artifacts use the following command.

    sudo make clobber

## Installation

hawkeye is packaged in an opkg format.  After cross compiling look in the opkg directory for an .opk file.  This is the file to be installed on the target system.

To install the package on the target, copy the file to the system or the SD card that is used to boo the system.  The use the following command.

    opkg install <path to file>/hawkeye_1.0_arm.opk

If the package has been installed previously, use the following command to reinstall it.

    opkg --force-reinstall install <path to file>/hawkeye_1.0_arm.opk

## Contributors

To get involved with PiBox, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

0BSD

